# Today I learned

## Accessing local server from another machine on the same network
Local server will be visible only locally (e.g. hosting with flask). Need to
explicitly specify to be visible externally. With Flask :
```
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
```
If the IP address of the machine is `192.168.X.X` then, from the same network 
you can access it in `5000` port. Like, `http://192.168.X.X:5000`

## Install Heroku on RPi
Heroku cannot be installed with `sudo snap install --classic heroku` as there
is no current distribution for the ARM chip. Kudos to [this thread](https://stackoverflow.com/questions/50492044/how-to-install-heroku-cli-on-raspberry-pi-3)
where using the below command, you can install heroku on the RPi
```
wget https://cli-assets.heroku.com/branches/stable/heroku-linux-arm.tar.gz
mkdir -p /usr/local/lib /usr/local/bin
sudo tar -xvzf heroku-linux-arm.tar.gz -C /usr/local/lib
sudo ln -s /usr/local/lib/heroku/bin/heroku /usr/local/bin/heroku
heroku update
```
